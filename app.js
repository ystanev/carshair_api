const express = require('express')
const cache = require('memory-cache');
const makes = require('./routes/makes')
const models = require('./routes/models')
const vinDecoder = require('./routes/vin_decoder')

const app = express()
const port = 3000

// configure cache middleware - https://www.digitalocean.com/community/tutorials/how-to-optimize-node-requests-with-simple-caching-strategies
let memCache = new cache.Cache();
let cacheMiddleware = (duration) => {
    return (req, res, next) => {
        let key = '__express__' + req.originalUrl || req.url
        let cacheContent = memCache.get(key);
        if (cacheContent) {
            res.send(cacheContent);
            return
        } else {
            res.sendResponse = res.send
            res.send = (body) => {
                memCache.put(key, body, duration * 1000);
                res.sendResponse(body)
            }
            next()
        }
    }
}

app.get('/', cacheMiddleware(30), makes)
app.get('/models', models)
app.get('/vindecoder', vinDecoder)

app.listen(port, () => {
    console.log(`Server listening on port ${port}`)
})
