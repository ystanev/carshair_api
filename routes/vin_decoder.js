const axios = require('axios')
const express = require('express')
const router = express.Router()

// http://127.0.0.1:3000/vindecoder?vin=1D4HB48D14F176699
router.get('/vindecoder', (request, response) => {
    // Given a VIN, get the year, make, and model of the vehicle (e.g. for 3N1AB6AP7BL729215: 2011 Nissan Sentra)
    const vin = request.query.vin
    axios.get(`https://vpic.nhtsa.dot.gov/api/vehicles/decodevinvalues/${vin}?format=json`)
        .then(res => {
            const result = `${res.data.Results[0].ModelYear} ${res.data.Results[0].Make} ${res.data.Results[0].Model}`
            // console.log(result)
            response.send(result)
        })
        .catch(error => {
            console.log(error)
        })
})

module.exports = router