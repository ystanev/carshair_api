const axios = require('axios')
const express = require('express')
const router = express.Router()

router.get('/', (request, response) => {
    // Get a list of all makes (e.g. Toyota, Honda, etc.)
    axios
        .get(`https://vpic.nhtsa.dot.gov/api/vehicles/getallmakes?format=json`)
        .then(res => {
            // console.log(`statusCode: ${res.status}`)
            makes = []
            res.data.Results.forEach(make => {
                makes.push(make.Make_Name)
            })
            // console.log(makes)
            response.send(JSON.stringify(makes))
        })
        .catch(error => {
            console.error(error)
        })
})

module.exports = router