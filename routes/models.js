const axios = require('axios')
const express = require('express')
const router = express.Router()

// http://127.0.0.1:3000/models?make=mazda&year=2019
router.get('/models', (request, response) => {
    // Get a list of all models given a make and year
    const make = request.query.make
    const year = request.query.year
    axios.get(`https://vpic.nhtsa.dot.gov/api/vehicles/getmodelsformakeyear/make/${make}/modelyear/${year}?format=json`)
        .then(res => {
            // console.log(res.data.Results)
            models = []

            res.data.Results.forEach(model => {
                models.push(model.Model_Name)
            })
            // console.log(models)
            response.send(JSON.stringify(models))
        })
        .catch(error => {
            console.log(error)
        })
})

module.exports = router