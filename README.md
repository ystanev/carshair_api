# Take Home Test carSHAiR

## Overview

The API has three routes that do the following:

- Get a list of all makes (e.g. Toyota, Honda, etc.)
- Get a list of all models given a make and year
- Given a VIN, get the year, make, and model of the vehicle (e.g. for 3N1AB6AP7BL729215: 2011 Nissan Sentra)

## Setup

- the use of **Arch Linux** is assumed for Node.js install

```bash
sudo pacman -Ss nodejs npm
```

- project setup/launch

```bash
npm install
node app.js
```

## Sample Routes

- the app runs on port **_3000_**
  - [http://127.0.0.1:3000/](http://127.0.0.1:3000/)
  - [http://127.0.0.1:3000/models?make=mazda&year=2019](http://127.0.0.1:3000/models?make=mazda&year=2019)
  - [http://127.0.0.1:3000/vindecoder?vin=1D4HB48D14F176699](http://127.0.0.1:3000/vindecoder?vin=1D4HB48D14F176699)
